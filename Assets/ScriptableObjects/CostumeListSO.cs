using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Profiles;

[CreateAssetMenu(fileName ="test",menuName ="SO/testCostume")]
public class CostumeListSO : ScriptableObject
{
    public List<Costume> costumesList;
}
