using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapController : MonoBehaviour
{
    public void PickMap1()
    {
        SceneManager.LoadScene("Gameplay");
        Time.timeScale = 1f;
    }    

    public void PickMap2()
    {
        SceneManager.LoadScene("map2");
        Time.timeScale = 1f;
    }    
 
  
}
