﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



/// <summary>
/// mapbuttonitem - gắn vào map button
/// để thực hiện 1 số hành động
/// </summary>

public class MapButtonItem : MonoBehaviour
{
    [HideInInspector] public int mapIndex;
    [HideInInspector] public MapScrollViewController mapScrollViewController;

    [SerializeField] Text mapButtonText;


    private void Start()
    {
        mapButtonText.text = "Map-" + (mapIndex + 1);
    }
    public void OnMapButtonClick()
    {
        mapScrollViewController.OnMapButtonClick(mapIndex);
        
    }

}
