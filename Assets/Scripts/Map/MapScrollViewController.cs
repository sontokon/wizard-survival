using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


/// <summary>
/// MapScrollViewController - generate scrollview items
/// handle all things those required for scroll view controller
/// </summary>
public class MapScrollViewController : MonoBehaviour
{

    [SerializeField] Text mapNumberText;
    [SerializeField] int numberOfMaps;
    [SerializeField] GameObject mapBtnPref;
    [SerializeField] Transform mapBtnParent;
    [SerializeField] Animation underDevAnim;

    public GameObject loadingScreen;
    public Slider loadingBarFill;


    private int mapNumber;

    private void Start()
    {
        LoadMapButton();
    }


    // load map btn on game start
    private void LoadMapButton()
    {
        for (int i = 0; i < numberOfMaps; i++)
        {
            GameObject mapBtnObj = Instantiate(mapBtnPref, mapBtnParent) as GameObject;
            mapBtnObj.GetComponent<MapButtonItem>().mapIndex = i;
            mapBtnObj.GetComponent<MapButtonItem>().mapScrollViewController = this;
        }
    }

    // user defined public method to handle smth when user press any button
    public void OnMapButtonClick(int mapIndex)
    {
        mapNumber = mapIndex;
        mapNumberText.text = "Map-" + (mapIndex + 1);
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + mapIndex +1);
        

    }




    


    public void PressPlay()
    {

        StartCoroutine(LoadSceneAsync(mapNumber + 1));

    }
   

    IEnumerator LoadSceneAsync(int mapNumbers)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(mapNumbers);
        loadingScreen.SetActive(true);
        while(!operation.isDone)
        {

            float progressValue = Mathf.Clamp01(operation.progress / 0.9f);
            loadingBarFill.value = progressValue;


            yield return null;
        }    

    }    
}
