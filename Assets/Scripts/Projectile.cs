using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    Rigidbody2D m_rb;
    //public MovementJoystick movementJoystick;
    [SerializeField] float projectileDamage = 10f ;
    [SerializeField] float timeToDestroy;

    private Transform enemy;
    private Vector2 target;
    [SerializeField] float speed;
    //public float splashDmg;
    public float splashRange;


    //[SerializeField] float speed;
    // Start is called before the first frame update

    private void Awake()
    {
        Destroy(gameObject, timeToDestroy);
    }
    void Start()
    {
        projectileDamage += PlayerAttack.Ins.playerBaseDamage;
        if(WeaponManager.weaponType == WeaponManager.WeaponType.FireBall && WeaponManager.weaponType == WeaponManager.WeaponType.LaserBall)
        {
            enemy = GameObject.FindGameObjectWithTag("Enemy").transform;

            target = new Vector2(enemy.position.x, enemy.position.y);

        }
        


        //transform.position = movementJoystick.joystickVec * speed * Time.deltaTime;
        //transform.position += transform.position;
        //m_rb = GetComponent<Rigidbody2D>();
        //Destroy(gameObject, timeToDestroy);
    }

    // Update is called once per frame
    void Update()
    {

        FindClosetEnemy();
        //transform.position = Vector2.MoveTowards(transform.position, target,speed * Time.deltaTime);
        //m_rb.AddForce()
        //Destroy(gameObject, timeToDestroy);
    }
    /*void OnBecameInvisible()
    {
        Destroy(gameObject);
    }*/


    /*private void OnCollisionEnter2D(Collision2D col)
    {
        print("as");
        if(col.gameObject.tag =="Enemy")
        {
            Debug.Log("hit enemy");
            EnemyAi enemy = col.gameObject.GetComponent<EnemyAi>();
            enemy.TakeDamage(projectileDamage);
            Destroy(gameObject);
        }
        
    }*/


    /*private void OnCollisionEnter2D(Collision2D col)
    {
        if(splashRange > 0)
        {
            var hitColliders = Physics2D.OverlapCircleAll(transform.position, splashRange);
            foreach (var hitCollider in hitColliders)
            {
                var enemy = hitCollider.GetComponent<EnemyAi>();
                if (enemy)
                {
                    var closetPoint = hitCollider.ClosestPoint(transform.position);
                    var distance = Vector3.Distance(closetPoint, transform.position);
                    enemy.TakeDamage(projectileDamage);
                    //Destroy(gameObject);
                }
            }
        }
        if (col.gameObject.tag == "Enemy")
        {
            //Debug.Log("hit enemy");
            EnemyAi enemy = col.gameObject.GetComponent<EnemyAi>();
            enemy.TakeDamage(projectileDamage);
            Destroy(gameObject);
        }
    }*/

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (splashRange > 0)
        {
            var hitColliders = Physics2D.OverlapCircleAll(transform.position, splashRange);
            foreach (var hitCollider in hitColliders)
            {
                var enemy = hitCollider.GetComponent<EnemyAi>();
                if (enemy)
                {
                    var closetPoint = hitCollider.ClosestPoint(transform.position);
                    var distance = Vector3.Distance(closetPoint, transform.position);
                    enemy.TakeDamage(projectileDamage);
                    //Destroy(gameObject);
                }
            }
        }
        if (col.gameObject.tag == "Enemy")
        {
            //Debug.Log("hit enemy");
            EnemyAi enemy = col.gameObject.GetComponent<EnemyAi>();
            enemy.TakeDamage(projectileDamage);
            Destroy(gameObject);
        }

    }


    void FindClosetEnemy()
    {
        float distanceToClosetEnemy = Mathf.Infinity;
        EnemyAi closetEnemy = null;
        EnemyAi[] allEnemies = GameObject.FindObjectsOfType<EnemyAi>();

        foreach (EnemyAi currentEnemy in allEnemies)
        {
            float distanceToEnemy = (currentEnemy.transform.position - this.transform.position).sqrMagnitude;
            if (distanceToEnemy < distanceToClosetEnemy)
            {
                distanceToClosetEnemy = distanceToEnemy;
                closetEnemy = currentEnemy;
            }
        }
        transform.position = Vector2.MoveTowards(transform.position, closetEnemy.transform.position, speed * Time.deltaTime);
        if(closetEnemy.transform.position == null)
            return;
        
            
        
        //Debug.DrawLine(this.transform.position, closetEnemy.transform.position);

    }







}
