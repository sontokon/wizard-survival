using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    public int damage;
    private void Start()
    {
        Destroy(gameObject, 3);
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            PlayerHealth.Ins.UpdateHealth(-damage);
            Destroy(gameObject);
        }    
    }
}
