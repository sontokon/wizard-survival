using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//[RequireComponent(typeof(SpriteRenderer))]
public abstract class EnemyAi : MonoBehaviour
{  
   // private SpriteRenderer spriteRenderer;
    //[SerializeField] Transform thisPlayer;
    [SerializeField] GameObject player;
    [SerializeField]float enemySpeed;
    bool facingRight;
    [SerializeField] float attackDamage = 1f;
    [SerializeField] private float attackSpeed = 1f;

    
    public GameController m_gc;

    float canAttack;
    public float normalExp = 10f;
    [SerializeField] int coins;

    public GameObject floatingTextPrefabs;

    public bool check;

    [Header("Health")]

    protected float health;
    [SerializeField] private float maxHealth;

    // ui kill
    UIManager m_ui;



    //private void Awake()
    //{
     //  this.spriteRenderer = this.GetComponent<SpriteRenderer>();
    //}
    // Start is called before the first frame update
    private void Start()
    {
        health = maxHealth;
        player = GameObject.Find("Player");
        m_gc = FindObjectOfType<GameController>();
      
    }
    
    public virtual void TakeDamage(float dmg)
    {
        ShowDamage(dmg.ToString());
        health -= dmg;
        //Debug.Log("Enemy health" + health);

        if(health <= 0)
        {
            PlayerLevel.instance.LevelUp(normalExp);

            SaveManager.Ins.CoinsIncrease(coins);
            m_gc.KillIncrement();
            Destroy(gameObject);
        }
        
    }

    // Update is called once per frame
    private void Update()
    {
        
        if (player == null)
        {
            return;
        }    
        //Vector2 direction = player.transform.position - transform.position;
        //direction.Normalize();
        transform.position = Vector2.MoveTowards(transform.position, player.transform.position, enemySpeed * Time.deltaTime);
        //this.spriteRenderer.flipX = thisPlayer.transform.position.x < this.transform.position.x;
        
        if(player.transform.position.x > gameObject.transform.position.x && facingRight)
        {
            Flip();

        }
        if(player.transform.position.x < gameObject.transform.position.x && !facingRight)
        {
            Flip();
        }
    }

    void Flip()
    {
        Vector3 currentScale = gameObject.transform.localScale;
        currentScale.x *= -1;
        gameObject.transform.localScale = currentScale;

        facingRight = !facingRight;
    }
    private void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (attackSpeed <= canAttack)
            {
                col.gameObject.GetComponent<PlayerHealth>().UpdateHealth(-attackDamage);
                canAttack = 0f;
            }
            else {
                canAttack += Time.deltaTime;
            }
        }
    }
    public void ShowDamage(string text)
    {
        if(floatingTextPrefabs)
        {
            GameObject prefab = Instantiate(floatingTextPrefabs, transform.position, Quaternion.identity);
            prefab.GetComponentInChildren<TextMesh>().text = text;
        }
    }



}
