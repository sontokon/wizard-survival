using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAI : EnemyAi
{
    
    public override void TakeDamage(float dmg)
    {
        ShowDamage(dmg.ToString());
        health -= dmg;

        //Debug.Log("Enemy health" + health);

        if (health <= 0)
        {

            GameController.PauseGame();
            m_gc.SetWinningState(true);


            PlayerLevel.instance.LevelUp(normalExp);
            Destroy(gameObject);
        }


    }

  


}
