using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchProjectile : MonoBehaviour
{
    public int numberOfProjectile;
    [SerializeField] GameObject enemyProjectile;
    Vector2 startPoint;
    float radius;
    float moveSpeed;
    public float timeSpawn = 2;
    float m_timeSpawn;

    private void Start()
    {
        m_timeSpawn = timeSpawn;
        radius = 5f;
        moveSpeed = 5f;
    }

    private void Update()
    {
        m_timeSpawn -= Time.deltaTime;
        if (m_timeSpawn <= 0)
         {
             startPoint = gameObject.transform.position;
             SpawnProjectile(numberOfProjectile);
             m_timeSpawn = timeSpawn;
         }
       

    }
    void SpawnProjectile(int numberOfProjectiles)
    {
        float angleStep = 360f / numberOfProjectiles; 
        float angle = 0f;
        for (int i = 0; i <= numberOfProjectiles - 1; i++)
        {
            float projectileDirXposition = startPoint.x + Mathf.Sin((angle * Mathf.PI) / 180) * radius;
            float projectileDirYPosition = startPoint.y + Mathf.Cos((angle * Mathf.PI) / 180) * radius;
            Vector2 projectileVector = new Vector2(projectileDirXposition, projectileDirYPosition);
            Vector2 projectileMoveDirection = (projectileVector - startPoint).normalized * moveSpeed;
            var proj = Instantiate(enemyProjectile, startPoint, Quaternion.identity);
            proj.GetComponent<Rigidbody2D>().velocity = new Vector2(projectileMoveDirection.x, projectileMoveDirection.y);
            angle += angleStep;

        }
    }
}
