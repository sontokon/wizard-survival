using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Singleton:UIManager
    public static UIManager Ins;

    private void Awake()
    {
        if (Ins == null)
        {
            Ins = this;

        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion


    public GameObject gameoverPanel;
    public GameObject winningPanel;
    public GameObject pausePanel;
    public Text coinsText;
    public Text levelText;
    public Text killText;
    public Text highestKillText;

    private void Start()
    {
        ShowHighestKill();
    }
    public void ShowGameoverPanel(bool isShow)
    {
        if(gameoverPanel)
        {
            gameoverPanel.SetActive(isShow);
        }    

    }
    public void ShowWinningPanel(bool isShow)
    {
        if (winningPanel)
        {
            winningPanel.SetActive(isShow);
            

        }

    }
    public void ShowPausePanel(bool isShow)
    {
        if (pausePanel)
        {
            pausePanel.SetActive(isShow);
        }

    }

    public void SetKillIncrement( string txt)
    {
        if(killText)
        {
            killText.text = txt;
        }    
    }    
    public void ShowHighestKill()
    {
        highestKillText.text = "Best Kill: " + PlayerPrefs.GetInt("HighestKill").ToString();
    }    

    public void SetLevelText(string txt)
    {
        if(levelText)
        {
            levelText.text = txt;
        }
    }

    public void ShowPlayerCoinsText(string txt)
    {
        coinsText.text = "Coins" + txt;
    }




}
