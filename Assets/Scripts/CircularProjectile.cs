using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularProjectile : MonoBehaviour
{
    [SerializeField] float projectileDamage = 5f;
    [SerializeField] Transform rotationCenter;
    [SerializeField] float rotationRadius = 2f , angularSpeed = 2f;
    float posX, posY, angle = 0f;


    private void Awake()
    {
        Destroy(gameObject, 3);
    }
    private void Start()
    {
        projectileDamage += PlayerAttack.Ins.playerBaseDamage;
        rotationCenter = GameObject.FindGameObjectWithTag("Player").transform;
        
    }
    void Update()
    {
        if(rotationCenter == null)
        {
            return;
        }    

        this.transform.position = rotationCenter.transform.position;
        
        posX = rotationCenter.position.x + Mathf.Cos(angle) * rotationRadius;
        posY = rotationCenter.position.y + Mathf.Sin(angle) * rotationRadius;
        transform.position = new Vector2(posX, posY);
        angle = angle + Time.deltaTime * angularSpeed;

        if(angle >= 360f)
        {
            angle = 0f;
        }
     }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy")
        {
           // Debug.Log("hit enemy");
            EnemyAi enemy = col.gameObject.GetComponent<EnemyAi>();
            enemy.TakeDamage(projectileDamage);
            //Destroy(gameObject);
        }

    }
}
