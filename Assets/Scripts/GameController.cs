using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    #region Singleton:GameController
    public static GameController Ins;

    private void Awake()
    {
        if (Ins == null)
        {
            Ins = this;

        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion



    public GameObject[] enemy;

    public GameObject[] SkillButton;

    public float spawnTime;
    float m_spawnTime;
    public float countingTimeNormalEnemy;
    float m_countingTimeNormalEnemy =20;



  
    public int kill;
    //boss time
   /* public float countingTimeBossEnemy;
    float m_countingTimeBossEnemy = 120;
    bool countingTimeBoss;*/



    UIManager m_ui;
    //public int spawnEnemy =3;
    bool m_isGameover;
    bool m_isWinning;




    // Start is called before the first frame update
    void Start()
    {
        m_ui = FindObjectOfType<UIManager>();
        m_ui.SetKillIncrement("kill: " + kill);

       
       


        countingTimeNormalEnemy = m_countingTimeNormalEnemy;


        StartCoroutine(TimeSpawnBoss());
        StartCoroutine(TimeSpawnEnemyDecrease());
      
        m_spawnTime = 0;
       

    }

    // Update is called once per frame
    void Update()
    {
        m_ui.SetKillIncrement("kill: " + kill);
        ActiveSkill();
        if (m_isGameover)
        {
            //m_spawnTime = 0;
            
            m_ui.ShowGameoverPanel(true);
            return;
        }    

        if(m_isWinning)
        {
            m_ui.ShowWinningPanel(true);
            return;
        }

        


        m_spawnTime -= Time.deltaTime;
        if (m_spawnTime <= 0)
        {
            SpawnEnemy();
            m_spawnTime = spawnTime;
        }
        countingTimeNormalEnemy -= Time.deltaTime;

        if(Input.GetKeyDown(KeyCode.S))
        {
            Instantiate(enemy[2], transform.position, Quaternion.identity);
        }    


        
        


    }

    public void ActiveSkill()
    {
        if (PlayerLevel.instance.playerLevel == WeaponUnlock.Ins.lvUnlockLightningBall)
        {
            SkillButton[0].SetActive(true);
        }
        if (PlayerLevel.instance.playerLevel == WeaponUnlock.Ins.lvUnlockLazer)
        {
            SkillButton[1].SetActive(true);
        }
        if (PlayerLevel.instance.playerLevel == WeaponUnlock.Ins.lvUnlockForceField)
        {
            SkillButton[2].SetActive(true);
        }
        if (PlayerLevel.instance.playerLevel == WeaponUnlock.Ins.lvUnlockLightningStrike)
        {
            SkillButton[3].SetActive(true);
        }
    }    

    public void SpawnEnemy()
    {
        float randXpos = Random.Range(-8.30f, 8.30f);
        float randYpos = Random.Range(-8.60f, 8.60f);
        Vector3 spawnPos = new Vector3(randXpos, randYpos, 0);

        if (enemy[0])
        {
            Instantiate(enemy[0], spawnPos, Quaternion.identity);
        }

        if(countingTimeNormalEnemy <= 0)
        {
            Instantiate(enemy[1], spawnPos, Quaternion.identity);
            countingTimeNormalEnemy = m_countingTimeNormalEnemy;
        }    
         
        

    }

    public void SpawnBoss()
    {

            Instantiate(enemy[2], transform.position, Quaternion.identity);

    }    

    public bool IsGameover()
    {
        return m_isGameover;
    }    

    public bool IsWinning()
    {
        return m_isWinning;
    }
    public void SetGameOverState(bool state)
    {
       m_isGameover = state;
    }  
    
    public void SetWinningState(bool state)
    {
        m_isWinning = state;
    }

    public void Replay()
    {
        SceneManager.LoadScene("Gameplay");
        Time.timeScale = 1f;
    }  

    public void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }


    //coroutine
    IEnumerator TimeSpawnEnemyDecrease()
    {
        yield return new WaitForSeconds(40f);
        spawnTime -= 0.5f;

    }
    IEnumerator TimeSpawnBoss()
    {
        yield return new WaitForSeconds(240f);
        SpawnBoss();
    }    

    //--------------------------------------


    public static void PauseGame()
    {
        Time.timeScale = 0;
    }
    public void PauseInGame()
    {
        Time.timeScale = 0;
        m_ui.ShowPausePanel(true);
    }

    public void Continues()
    {
        Time.timeScale = 1f;
        m_ui.ShowPausePanel(false);
    }
    public void KillIncrement()
    {
        
        kill++;
        m_ui.SetKillIncrement("kill: " + kill);
        SaveManager.Ins.SaveHighestKill(kill);
    }    

    
  
}
