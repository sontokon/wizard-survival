using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyInSecond : MonoBehaviour
{
    [SerializeField] private float timeDestroy = 2f;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, timeDestroy);
    }

  
}
