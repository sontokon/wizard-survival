using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JustForTestingSave : MonoBehaviour
{
    public bool isPurchased ;
    public bool test1;


    private void Start()
    {
        SaveData();
        LoadData();
    }


    public void SaveData()
    {
        PlayerPrefs.SetInt("JustTestSaving", isPurchased ? 1 : 0);
        PlayerPrefs.SetInt("test1", test1 ? 1 : 0);
        PlayerPrefs.Save();
    }

    public void LoadData()
    {
        isPurchased = PlayerPrefs.GetInt("JustTestSaving") == 0 ? true : false;
        test1 = PlayerPrefs.GetInt("test1")== 1 ? true : false;
    }
    
}
