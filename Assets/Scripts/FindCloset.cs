using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindCloset : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FindClosetEnemy();
    }
    void FindClosetEnemy()
    {
        float distanceToClosetEnemy = Mathf.Infinity;
        EnemyAi closetEnemy = null;
        EnemyAi[] allEnemies = GameObject.FindObjectsOfType<EnemyAi>();

        foreach(EnemyAi currentEnemy in allEnemies)
        {
            float distanceToEnemy = (currentEnemy.transform.position - this.transform.position).sqrMagnitude;
            if(distanceToEnemy <distanceToClosetEnemy)
            {
                distanceToClosetEnemy = distanceToEnemy;
                closetEnemy = currentEnemy;
            }
        }
        if (closetEnemy == null) return;
        Debug.DrawLine(this.transform.position, closetEnemy.transform.position);

    }
}
