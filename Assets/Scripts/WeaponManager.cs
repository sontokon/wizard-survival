using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{

    public enum WeaponType
    {
        FireBall,
        LaserBall,
        LightningStrike,
        ForceField,
        LightningBall
    }

    public static WeaponType weaponType;

    public static WeaponType addWeapon;


    private void Awake()
    {
        weaponType = WeaponType.FireBall;
    }

    public void ChangeWeapon()
    {
        if (weaponType == WeaponType.FireBall)
        {
            weaponType = WeaponType.LaserBall;
        }
        else {
            weaponType = WeaponType.FireBall;
        }


    }
    



}
