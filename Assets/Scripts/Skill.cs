using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Skill",menuName = "Skills" )]
public class Skill : ScriptableObject
{
    public string projectileName;

    public float dmg;
    public float fireSpeed;
    public float area;

}
