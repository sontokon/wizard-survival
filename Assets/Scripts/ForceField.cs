using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceField : MonoBehaviour
{
    [SerializeField] float projectileDamage = 10f;
    [SerializeField] Transform player;
    private Vector2 playerPos;
    // Start is called before the first frame update
    private void Awake()
    {
        Destroy(gameObject, 3f);
    }
    void Start()
    {

        player = GameObject.FindGameObjectWithTag("Player").transform;
        this.transform.position = player.transform.position;
        

    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            return;
        }

        this.transform.position = player.transform.position;
        //playerPos = new Vector2(player.position.x, player.position.y);
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            //Debug.Log("hit enemy");
            EnemyAi enemy = col.gameObject.GetComponent<EnemyAi>();
            enemy.TakeDamage(projectileDamage);
          
        }

    }
    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            //Debug.Log("hit enemy");
            EnemyAi enemy = col.gameObject.GetComponent<EnemyAi>();
            enemy.TakeDamage(projectileDamage);

        }
    }
}
