using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public MovementJoystick movementJoystick;
    public GameObject[] projectile ;
    public GameObject[] nonShootingProjectile;
    public Transform shootingPoint;
    
    public WeaponUnlock weaponUnlock;


    //audio
    public AudioSource aus;
    public AudioClip fireBallSound;
    public AudioClip lazerBallSound;
    public AudioClip lightningStrikeSound;


    //public float fireSpeed = 100f;

    public float playerBaseDamage;
    public float fireRate = 1f;
    private float m_fireRate;
    bool m_isShooted;

    private bool allowShot;

    public float playerLevel = 0f;

    //spawn time of non moving projectile
    [Header("spawn time of non moving projectile")]
    public float spawnTimeProj = 3f;
    float m_spawnTimeProj;



    public static PlayerAttack Ins;

    private void Awake()
    {
        playerBaseDamage = PlayerPrefs.GetFloat("PlayerBaseDamage");
        m_fireRate = fireRate;
        m_spawnTimeProj = 0f;
        if (Ins == null)
        {
            Ins = this;

        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        allowShot = false;
        
        //SpawnNonMovingProjectile();

        //weaponUnlock = GetComponent<WeaponUnlock>();
        //nonShootingProjectile[0].transform.position = transform.position;

    }

    // Update is called once per frame
    void Update()
    {

        m_spawnTimeProj -= Time.deltaTime;
        if(m_spawnTimeProj <=0)
        {
            SpawnNonMovingProjectile();
            m_spawnTimeProj = spawnTimeProj;
        }    
        
        //set active for projectile
     /*   if (Input.GetKeyDown("space"))
        {
            //SetActiveSkill();
            allowShot = true; 
        }
*/
        //SpawnNonMovingProjectile();
        
        

        if (!m_isShooted)
        {
            Shoot();
            SpawnLightningStrike();
            
        }
        if (m_isShooted)
        {
            m_fireRate -= Time.deltaTime;
            if (m_fireRate <= 0)
            {
                m_isShooted = false;
                m_fireRate = fireRate;
            }

        }
    }
        //float _nextFire;
        //Vector2 lastDir = Vector2.right;
         void Shoot()
        {
       
            m_isShooted = true;
            if(WeaponManager.weaponType == WeaponManager.WeaponType.FireBall)
            {
                if(aus&&fireBallSound)
                {
                aus.PlayOneShot(fireBallSound);
                }
                        Instantiate(projectile[0], shootingPoint.position, shootingPoint.rotation);

            }
            if (weaponUnlock.unlockLaser == true)
            {
                if (aus && lazerBallSound)
                {
                    aus.PlayOneShot(lazerBallSound);
                }
                    Instantiate(projectile[1], shootingPoint.position, shootingPoint.rotation);

            }

       
         }
    void SpawnNonMovingProjectile()
    {
        if(weaponUnlock.unlockLightningBall == true)
        {
            //Debug.Log("da ban");
            
            
            
            Instantiate(nonShootingProjectile[0], shootingPoint.position, Quaternion.identity);

        }
       if(weaponUnlock.unlockForceField == true)
        {
            Instantiate(nonShootingProjectile[1], shootingPoint.position, Quaternion.identity);
        }
        
        
    }

    void SpawnLightningStrike()
    {
        if(weaponUnlock.unlockLightningStrike == true )
        {
            m_isShooted = true;
            float randXpos = Random.Range(-8.30f, 8.30f);
            float randYpos = Random.Range(-8.60f, 8.60f);
            Vector3 spawnPos = new Vector3(randXpos, randYpos, 0);
            aus.PlayOneShot(lightningStrikeSound);
            Instantiate(nonShootingProjectile[2], spawnPos, Quaternion.identity);
        }
       
    }


        /*void SetActiveSkill()
        {
            for(int i = 0; i<2;i++)
            {
            projectile[i].SetActive(true);
            }
        }*/
    
}
