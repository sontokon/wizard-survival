using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLevel : MonoBehaviour
{
    public static PlayerLevel instance;
    [SerializeField] private float restoreHP = 20f;
    [SerializeField] private float upMaxHealth = 100f;
    [SerializeField] private float curPlayerExp;
    [SerializeField] public int playerLevel;
    [SerializeField] private float levelUpExp;
    [SerializeField] private Slider expSlider;

    UIManager m_ui;
    private void Awake()
    {
        instance = this;
    }
    

    // Start is called before the first frame update
    void Start()
    {
        curPlayerExp = 0f;
        playerLevel = 0;
        levelUpExp = 100f;
        expSlider.value = curPlayerExp;
        expSlider.maxValue = levelUpExp;

        m_ui = FindObjectOfType<UIManager>();
        m_ui.SetLevelText("Level: " + playerLevel);
    }
    private void Update()
    {
        expSlider.value = curPlayerExp;
        expSlider.maxValue = levelUpExp;
        m_ui.SetLevelText("Level: " + playerLevel);

        //playerLevel;
    }


    public void LevelUp(float exp)
    {
        curPlayerExp += exp;
        

        if (curPlayerExp >= levelUpExp)
        {
            //upMaxHealth += upMaxHealth;
            PlayerHealth.Ins.UpdateHealth(+restoreHP);
            PlayerHealth.Ins.IncreaseHealthOnLvUP(+upMaxHealth);

            playerLevel++;
            curPlayerExp = 0;
            levelUpExp += levelUpExp;
            expSlider.value = curPlayerExp;
        }
    }


    public void ShowLevelText()
    {
        m_ui.SetLevelText("Level: " + playerLevel);
    }
    public void CheckPlayerLevel(int curPlayerLevel)
    {
        curPlayerLevel = playerLevel;
    }    

    private void OnGUI()
    {
        float t = Time.deltaTime / 1f;
        expSlider.value = Mathf.Lerp(expSlider.value, levelUpExp, t);
    }
}
    
