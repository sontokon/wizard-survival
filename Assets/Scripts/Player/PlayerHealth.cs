using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    float health = 0f;
    [SerializeField] float maxHealth = 100f;
    [SerializeField] private Slider healthSlider;

    public GameController m_gc;
    public static PlayerHealth Ins;

    private void Awake()
    {
        Ins = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        healthSlider.maxValue = maxHealth;
        m_gc = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateHealth(float mod)
    {
        health += mod;
        if(health > maxHealth)
        {
            health = maxHealth;
        }else if(health <=0f)
        {
            health = 0f;
            healthSlider.value = health;
            m_gc.SetGameOverState(true);
            Destroy(gameObject);
        }
    }
    public void IncreaseHealthOnLvUP(float upMaxHealth)
    {
        maxHealth += upMaxHealth;
    }    
    private void OnGUI()
    {
        float t = Time.deltaTime / 1f;
        healthSlider.value = Mathf.Lerp(healthSlider.value, health, t);
    }
}
