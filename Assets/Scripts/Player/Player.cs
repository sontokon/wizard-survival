using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public MovementJoystick movementJoystick;
    public float playerSpeed;
    private Rigidbody2D m_rb;





    //bool facingRight = true;
    //[SerializeField] Rigidbody2D projectile;
    //public Transform shootingPoint;
    //public float fireRate=100f;

   
    // Start is called before the first frame update
    void Start()
    {
        m_rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(movementJoystick.joystickVec.y !=0)
        {
            m_rb.velocity = new Vector2(movementJoystick.joystickVec.x * playerSpeed, movementJoystick.joystickVec.y *playerSpeed);

        }
        else
        {
            m_rb.velocity = Vector2.zero;
        }
      /*
        if(movementJoystick.joystickVec.x >0 && !facingRight )
        {
            Flip();
        }

        if(movementJoystick.joystickVec.x <0 && facingRight)
        {
            Flip();
        }
      */

        if(movementJoystick.joystickVec.x >0)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }else if(movementJoystick.joystickVec.x<0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }
    /*private void Update()
    {
        
   
         Shoot();
       
       
    }*/

    

    /*void Flip()
    {
        Vector3 currentScale = gameObject.transform.localScale;
        currentScale.x *= -1;
        gameObject.transform.localScale = currentScale;

        facingRight = !facingRight;

    }*/

    /*public void Shoot()
    {
        if(projectile && shootingPoint)
        {
          var fireProjectile = Instantiate(projectile, shootingPoint.position, shootingPoint.rotation);
            fireProjectile.AddForce(movementJoystick.joystickVec * fireRate);
         
        }
    }*/
}
