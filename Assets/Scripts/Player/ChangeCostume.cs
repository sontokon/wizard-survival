using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCostume : MonoBehaviour
{
    public CostumeListSO costumeListSO;
    
    public SpriteRenderer image;
    private void Start()
    {
        
        
        for(int i =0; i< costumeListSO.costumesList.Count;i++)
        {
            
            if(costumeListSO.costumesList[i].selectionCostume == true)
            {
                image.sprite = costumeListSO.costumesList[i].Image;
            }
          
           
        }    
    }
}
