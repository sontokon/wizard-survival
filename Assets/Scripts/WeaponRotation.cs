using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponRotation : MonoBehaviour
{
    public MovementJoystick movementJoystick;
    public float angle;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void Update()
    {
        RotateWeapon();
    }

    // Update is called once per frame
    void RotateWeapon()
    {
        if (movementJoystick.joystickVec == Vector2.zero) return;
        angle = Mathf.Atan2(movementJoystick.joystickVec.y, movementJoystick.joystickVec.x) * Mathf.Rad2Deg;
        var lookRotation = Quaternion.Euler(angle * Vector3.forward);
        transform.rotation = lookRotation;
        transform.localScale = movementJoystick.joystickVec.x > 0 ? Vector3.one : new Vector3(1, -1, -1);

    }
}
