using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Utilities
{
    public sealed class ShowFPS : MonoBehaviour
    {
        private float _timer, _refresh, _avgFramerate;
        private const string Display = "{0} FPS";
        [SerializeField] private Text targetDisplay;
        [SerializeField] private TextMeshProUGUI targetDisplayTms;

        private delegate void Show();

        private Show _show;

        private float _timelapse;

        // Update is called once per frame
        private void Start()
        {
            if (targetDisplay)
            {
                _show += NormalText;
                return;
            }

            _show += TMP;
        }


        private void Update()
        {
            _show();
        }

        private void NormalText()
        {
            _timelapse = Time.unscaledDeltaTime;
            _timer = _timer <= 0 ? _refresh : _timer -= _timelapse;

            if (_timer <= 0) _avgFramerate = (int)(1f / _timelapse);
            targetDisplay.text = string.Format(Display, _avgFramerate.ToString(CultureInfo.InvariantCulture));
        }

        private void TMP()
        {
            _timelapse = Time.unscaledDeltaTime;
            _timer = _timer <= 0 ? _refresh : _timer -= _timelapse;

            if (_timer <= 0) _avgFramerate = (int)(1f / _timelapse);
            targetDisplayTms.text = string.Format(Display, _avgFramerate.ToString(CultureInfo.InvariantCulture));
        }
    }
}