using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingSave : MonoBehaviour
{
    public Inven inven = new Inven();
    List<Items> itemsabc = new List<Items>();

    private void Start()
    {
        itemsabc = inven.items;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.S))
        {
            itemsabc[0].name = "asdasd13246";
            SaveToJson();
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadFromJson();
        }
    }
    public void SaveToJson()
    {
        //List<Items> itemsabc = new List<Items>();

        //inven.items = itemsabc;
        string invenData = JsonUtility.ToJson(inven);
        string filePath = Application.persistentDataPath + "/InvenTest.json";

        Debug.Log(filePath);
        System.IO.File.WriteAllText(filePath, invenData);
        Debug.Log("save");
        

    }
    public void LoadFromJson()
    {
        string filePath = Application.persistentDataPath + "/InvenTest.json";
        string invenData = System.IO.File.ReadAllText(filePath);

        inven = JsonUtility.FromJson<Inven>(invenData);
        Debug.Log("Loaded");
    }

}
[System.Serializable] public class Inven
{
    public int goldCoins;
    public bool isFull;
    public List<Items> items = new List<Items>();
}

[System.Serializable] public class Items
{
    public string name;
    public string desc;
    //public Sprite image;
    public bool isPurchased;
}
