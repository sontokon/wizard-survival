using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public static SaveManager Ins;

    public int playerCoins;

    
    private void Start()
    {
        playerCoins =PlayerPrefs.GetInt("PlayerCoins");
        Debug.Log("coin" + PlayerPrefs.GetInt("PlayerCoins",playerCoins));
        Debug.Log(PlayerPrefs.GetInt("HighestKill"));
    }



    private void Awake()
    {
        Ins = this;
    }
    public void SaveHighestKill(int highestKill)
    {
       if(highestKill > PlayerPrefs.GetInt("HighestKill",0 ))
        {
            PlayerPrefs.SetInt("HighestKill", highestKill);
        }
        
    }    

    public void SavePlayerCoins(int coins)
    {
        playerCoins += coins;
        PlayerPrefs.SetInt("PlayerCoins", playerCoins);

    }

    
   /* public void SavePlayerBaseDmg(float baseDmg)
    {
        PlayerPrefs.SetFloat("PlayerBaseDmg", baseDmg);
    }
*/
    public void CoinsIncrease(int amount)
    {
        playerCoins += amount;
        SaveManager.Ins.SavePlayerCoins(amount);
        UIManager.Ins.ShowPlayerCoinsText(playerCoins.ToString());
    }


}
