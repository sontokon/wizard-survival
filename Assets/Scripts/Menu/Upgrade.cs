using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Upgrade : MonoBehaviour
{
    [SerializeField] Text coinsNeedText;
    [SerializeField] Game _game;


    float dmg = 10 ;
    int coinsNeed = 250;




    private void Start()
    {
 
        coinsNeedText.text = coinsNeed.ToString();
        _game = FindObjectOfType<Game>();
    }

    public void UpDmg()
    {
        if(_game.Coins >= coinsNeed)
        {
            
            _game.playerBaseDmg += dmg;
            Debug.Log(_game.playerBaseDmg);
            _game.SaveDamage(_game.playerBaseDmg);
            _game.UseCoins(coinsNeed);
            Debug.Log(_game.Coins);
            Game.Ins.UpdateAllCoinsUIText();
        }


        
    }

    public void DecreaseDmg()
    {
        if (_game.Coins >= coinsNeed)
        {
            _game.playerBaseDmg -= dmg;
            Debug.Log(_game.playerBaseDmg);
            _game.SaveDamage(_game.playerBaseDmg);
            _game.AddCoins(coinsNeed);
            Game.Ins.UpdateAllCoinsUIText();
        }
    }    

    public void OpenUpgrade()
    {
        gameObject.SetActive(true);
    }

    public void CloseUpGrade()
    {
        gameObject.SetActive(false);
    }

}
