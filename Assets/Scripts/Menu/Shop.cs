﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Shop : MonoBehaviour
{

    #region Singleton:Shop

    public static Shop Ins;

    private void Awake()
    {
        if (Ins == null)
        {
            Ins = this;

        }
        else
        {
            Destroy(gameObject);
        }

        //LoadFromJson();
    }

    #endregion

    [System.Serializable] public class ShopItem
    {
        public Sprite Image;
        public int Price;
        public bool IsPurchased = false; // kiem tra xem da mua chua


    }

    [System.Serializable] public class DataItems
    {
        public List<ShopItem> dataItems = new List<ShopItem>();
    }

    public DataItems dataItemsA;
 
    public List<ShopItem> ShopItemList ;
 


    [SerializeField] Animator NoCoinsAnim;


   
    [SerializeField] GameObject ItemTemplate;
    GameObject g;
    [SerializeField] GameObject shopPanel;
    [SerializeField]Transform ShopScrollView;
    Button buyBtn;

    


    private void Start()
    {
       
        //testing Save
        dataItemsA.dataItems = ShopItemList;
        if (!PlayerPrefs.HasKey("firstTime")) //return true if the key exist
        {
            print("First time in the game.");

            PlayerPrefs.SetInt("firstTime", 0);
            SaveToJson();//to save a key with a value 0
                         //then came into being for the next time
        }
        else
        {
            print("It is not the first time in the game.");
            LoadFromJson();
            //because the key "firstTime"
        }

        //LoadFromJson();
        //end Testing

        //ItemTemplate = ShopScrollView.GetChild(0).gameObject;
        //XmlIO.GetPlatform<ShopItem>("data2Test");
        int len = ShopItemList.Count;

        for(int i =0; i< len;i++)
        {
            g = Instantiate(ItemTemplate, ShopScrollView);
            g.transform.GetChild(0).GetComponent<Image>().sprite = ShopItemList [i].Image;
            g.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = ShopItemList [i].Price.ToString();
            buyBtn = g.transform.GetChild(2).GetComponent<Button>();

            if(ShopItemList[i].IsPurchased)
            {
                DisableBuyButton();

            }

            //buyBtn.interactable = !ShopItemList[i].IsPurchased;
            buyBtn.AddEvenListner(i,OnShopItemBtnClicked);
        }

        




    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.A))
        {
            SaveToJson();
        }

        if(Input.GetKeyDown(KeyCode.D))
        {
            LoadFromJson();
        }
    }


    void OnShopItemBtnClicked(int itemIndex)
    {
        Debug.Log(itemIndex);

        if (Game.Ins.HasEnoughCoins(ShopItemList[itemIndex].Price))
        {
            Game.Ins.UseCoins(ShopItemList[itemIndex].Price);
            //purchase item
            ShopItemList[itemIndex].IsPurchased = true;

            //disable the button
            buyBtn = ShopScrollView.GetChild(itemIndex).GetChild(2).GetComponent<Button>();
            DisableBuyButton();

            //Change UI Text coins
            Game.Ins.UpdateAllCoinsUIText();


            //add costume
            Profiles.Ins.AddCostume(ShopItemList[itemIndex].Image);

            //SaveCostume
            //XmlIO.SetPlatform<ShopItem>("data2Test", ShopItemList[itemIndex]);
            //PlayerPrefs.SetInt("")
            SaveToJson();
        }
        else
        {
            NoCoinsAnim.SetTrigger("NoCoins");
            Debug.Log("not enough money");
        }
        
    }

    void DisableBuyButton()
    {
        buyBtn.interactable = false;
        buyBtn.transform.GetChild(0).GetComponent<Text>().text = "PURCHASED";
    }

    /*-----------Shop Coins UI---------*/


    //Save to Json
    public void SaveToJson()
    {
        string ivenData = JsonUtility.ToJson(dataItemsA);
        string filePath = Application.persistentDataPath + "/itemShopData.json";
        Debug.Log(filePath);
        System.IO.File.WriteAllText(filePath, ivenData);
        
        Debug.Log("Saved");
    }

    public void LoadFromJson()
    {
        string filePath = Application.persistentDataPath + "/itemShopData.json";
        string ivenData = System.IO.File.ReadAllText(filePath);
        dataItemsA = JsonUtility.FromJson<DataItems>(ivenData);

        ShopItemList = dataItemsA.dataItems;
        Debug.Log("Loaded");
    }


    //open and close

    public void OpenShop()
    {
        shopPanel.SetActive(true);
    }

    public void CloseShop()
    {
        shopPanel.SetActive(false);
    }
    public void LoadData()
    {
        LoadFromJson();
    }
}
