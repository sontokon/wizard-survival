using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Profiles : MonoBehaviour
{
    #region Singleton:Profiles

    public static Profiles Ins;

    private void Awake()
    {
        if(Ins == null)
        {
            Ins = this;

        }
        else
        {
            Destroy(gameObject);
        }
    }

    #endregion

    
    [System.Serializable]public class Costume
    {
        public Sprite Image;
        public bool selectionCostume;
    }

    public CostumeListSO costumeListSO;

    [SerializeField] GameObject CostumeUITemplate;
    [SerializeField] Transform CostumeScrollView;

    GameObject g;
    int newSelectedIndex, previousSelectedIndex;

    [SerializeField] Color ActivatedCostumeColor;
    [SerializeField] Color DefaultCostumeColor;

    [SerializeField] Image currentCostume;


    private void Start()
    {
        costumeListSO.costumesList = new List<Costume>();
        GetAvailableCostumes();
        newSelectedIndex = previousSelectedIndex = 0;
        costumeListSO.costumesList[previousSelectedIndex].selectionCostume = true;
    }

    private void GetAvailableCostumes()
    {
 
        for(int i = 0; i < Shop.Ins.ShopItemList.Count;i++)
        {
            if(Shop.Ins.ShopItemList[i].IsPurchased)
            {
                AddCostume(Shop.Ins.ShopItemList[i].Image);
            }
        }

        SelectCostume(newSelectedIndex);
    }
    public void AddCostume(Sprite img)
    {
        if(costumeListSO.costumesList == null)
        {
            costumeListSO.costumesList = new List<Costume>();
        }
        Costume cs = new Costume(){Image = img};
        
        //add costume to costumeList
        costumeListSO.costumesList.Add(cs);

        //add costume in the ui scroll view
        g = Instantiate(CostumeUITemplate, CostumeScrollView);

        g.transform.GetChild(0).GetComponent<Image>().sprite = cs.Image;

        // add click event

        g.transform.GetComponent<Button>().AddEvenListner(costumeListSO.costumesList.Count - 1, OnCostumeClick);
     }

    private void OnCostumeClick(int CostumeIndex)
    {
        SelectCostume(CostumeIndex);
    }

    void SelectCostume(int CostumeIndex)
    {
        //costumeListSO.costumesList[previousSelectedIndex].selectionCostume = false;
        previousSelectedIndex = newSelectedIndex;
        costumeListSO.costumesList[previousSelectedIndex].selectionCostume = false;
        newSelectedIndex = CostumeIndex;
        
        CostumeScrollView.GetChild(previousSelectedIndex).GetComponent<Image>().color = DefaultCostumeColor ;
        CostumeScrollView.GetChild(newSelectedIndex).GetComponent<Image>().color = ActivatedCostumeColor;

        // Change Costume
        currentCostume.sprite = costumeListSO.costumesList[newSelectedIndex].Image;
        costumeListSO.costumesList[newSelectedIndex].selectionCostume = true;
    }

    public void OpenProfile()
    {
        gameObject.SetActive(true);
    }

    public void CloseProfile()
    {
        gameObject.SetActive(false);
    }

    
}
