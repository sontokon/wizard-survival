using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject BestKillPanel;
    public GameObject MapPanel;
    public Text bestKill;

    private void Start()
    {
        bestKill.text = PlayerPrefs.GetInt("HighestKill").ToString();
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = -1;

    }
    public void PlayGame()
    {
        /*SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); //load scene gameplay
        Time.timeScale = 1f;*/
        MapPanel.SetActive(true);
    }
    public void OpenBestKillPanel()
    {
        BestKillPanel.SetActive(true);
    }
    public void ExitBestKillPanel()
    {
        BestKillPanel.SetActive(false);
    }

    
     
    
}
