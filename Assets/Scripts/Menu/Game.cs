using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    #region SIngleton:Game
    public static Game Ins;
    private void Awake()
    {
        if(Ins == null)
        {
            Ins = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        playerBaseDmg = PlayerPrefs.GetFloat("PlayerBaseDamage");
    }


    #endregion

    [SerializeField] Text[] allCoinsUIText;

    public int Coins;
    public float playerBaseDmg = 10;

    

    private void Start()
    {
        Coins = PlayerPrefs.GetInt("PlayerCoins");
        //PlayerPrefs.SetFloat("PlayerBaseDamage", playerBaseDmg);
        Debug.Log(playerBaseDmg);

        UpdateAllCoinsUIText();
    }


    public void UseCoins(int amount)
    {
        Coins -= amount;
        PlayerPrefs.SetInt("PlayerCoins",Coins);
    }

    public bool HasEnoughCoins(int amount)
    {
        return (Coins >= amount);
    }

    public void UpdateAllCoinsUIText()
    {
        for (int i = 0 ; i < allCoinsUIText.Length; i++)
        {
            allCoinsUIText[i].text = Coins.ToString();
        }
    }

    public void SaveDamage(float dmg)
    {
        playerBaseDmg = dmg;
        PlayerPrefs.SetFloat("PlayerBaseDamage", playerBaseDmg);
    }    

    public void SaveUpgradeDmg(float upDmg)
    {
        playerBaseDmg += upDmg;
        PlayerPrefs.SetFloat("PlayerBaseDamage", playerBaseDmg);

    }


    // for testting
    public void AddCoins(int amount)
    {
        Coins += amount;
        PlayerPrefs.SetInt("PlayerCoins", Coins);
    }
}
