﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public static class ButtonExtension
{
   public static void AddEvenListner<T>(this Button button,T param,Action<T> Onclick )
    {
        //Delegate là một biến kiểu tham chiếu chứa tham chiếu tới một phương thức. Tham chiếu đó có thể được thay đổi tại runtime.
        button.onClick.AddListener(delegate () { 
            Onclick(param); 
        });
    }

}
