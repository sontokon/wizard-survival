using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponUnlock : MonoBehaviour
{

    public static WeaponUnlock Ins;
    

    public bool unlockLaser;
    public bool unlockForceField;
    public bool unlockLightningBall;
    public bool unlockLightningStrike;


    //lv can unlock skill

    public int lvUnlockLazer = 2;
    public int lvUnlockForceField = 5;
    public int lvUnlockLightningBall = 1;
    public int lvUnlockLightningStrike = 4;

    private void Awake()
    {
        Ins = this;
    }



    public void UnlockLazer()
    {   
        if(PlayerLevel.instance.playerLevel >= lvUnlockLazer)
        {
            unlockLaser = true;
        }    
        
       
    }    
    public void UnlockForceField()
    {

        if (PlayerLevel.instance.playerLevel >= lvUnlockForceField)
            unlockForceField = true;
        

    }   
    public void UnlockLighningBall()
    {
        if (PlayerLevel.instance.playerLevel >= lvUnlockLightningBall)
            unlockLightningBall = true;
     
    }
    public void UnlockLightningStrike()
    {
        if (PlayerLevel.instance.playerLevel >= lvUnlockLightningStrike)
            unlockLightningStrike = true;
        
            
     }

}
